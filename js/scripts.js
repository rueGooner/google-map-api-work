document.addEventListener('DOMContentLoaded', function() {
    var map;
    var yard;
    // get users location if they allow it
    function usersLocation() {
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(getLatLong);
        } else {
            alert('Geolocation is not supported by this browser');
        }
    }
    // get Lat and Long values
    function getLatLong(position) {
        test.innerHTML = position.coords.latitude + "<br>" + position.coords.longitude;
    }
    // on click fun above functions
    document.getElementById('getLocation').addEventListener("click", function() {
        usersLocation();
    });
    // generate google maps
    // map properties
    function initMap() {
        // yard = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
        yard = new google.maps.LatLng(53.534247699999995,-2.3633115);
        var mapProperties = {
            center: yard,
            zoom: 17,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("mapCanvas"), mapProperties);
    }
    google.maps.event.addDomListener(window, 'load', initMap);

});
